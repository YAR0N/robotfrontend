#include "RobotFrontend.h"
#include "ui_RobotFrontend.h"
#include <QInputDialog>
#include <QMessageBox>
#include <QKeyEvent>
#include <QDir>
#include <QDirIterator>

RobotFrontend::RobotFrontend(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RobotFrontend),
    rs(new RobotSocket)
{
    ui->setupUi(this);
    scene = new QGraphicsScene(this);
    ui->Map->setScene(scene);

    connect(rs, SIGNAL(connected()), this, SLOT(handleConnection()));
    connect(rs, SIGNAL(disconnected()), this, SLOT(handleDisconnection()));
    connect(rs, SIGNAL(socketError(QAbstractSocket::SocketError)), this, SLOT(showError(QAbstractSocket::SocketError)));
    connect(rs, SIGNAL(messageReceived(QString)), ui->Messages, SLOT(setText(QString)));
    connect(rs, SIGNAL(mapDataReceived(int,int)), this, SLOT(drawMap(int,int)));
    connect(rs, SIGNAL(robotDataReceived(int,int)), this, SLOT(drawRobotPos(int,int)));

    handleDisconnection();
}

RobotFrontend::~RobotFrontend()
{
    delete ui;
}

void RobotFrontend::enableButtons(bool enable, bool enableStop)
{
    ui->Scan->setEnabled(enable);
    ui->Back->setEnabled(enable);
    ui->Front->setEnabled(enable);
    ui->Left->setEnabled(enable);
    ui->Right->setEnabled(enable);

    ui->Stop->setEnabled(enableStop);
}

void RobotFrontend::handleConnection()
{
    enableButtons(true, true);
    ui->Connect->setText("Disconnect");
    ui->ConnectionStatus->setText("Connected");
    ui->ConnectionStatusCircle->setStyleSheet("background-color: green; border: 2px solid black; border-radius: 50px;");
}

void RobotFrontend::handleDisconnection()
{
    enableButtons(false, false);
    ui->Connect->setText("Connect");
    ui->ConnectionStatus->setText("Disconnected");
    ui->ConnectionStatusCircle->setStyleSheet("background-color: red; border: 2px solid black; border-radius: 50px;");
}

void RobotFrontend::showError(QAbstractSocket::SocketError error)
{
    qDebug()<<error;
    ui->Messages->setText("Critical socket error");
}

void RobotFrontend::drawMap(int x, int y)
{
    qDebug()<<"Map: x - "<<x<<" / y - "<<y<<"\n";
    QBrush brush(Qt::black);
    QPen outlinePen(Qt::black);
    outlinePen.setWidth(0);
    scene->addEllipse(x*5, y*5, 1, 1, outlinePen, brush);
}

void RobotFrontend::drawRobotPos(int x, int y)
{
    qDebug()<<"Robot: x - "<<x<<" / y - "<<y<<"\n";
    QBrush brush(Qt::red);
    QPen outlinePen(Qt::red);
    outlinePen.setWidth(0);
    scene->addEllipse(x*5, y*5, 1, 1, outlinePen, brush);
}

void RobotFrontend::on_Front_pressed()
{
    rs->sendCommand(RobotSocket::FRONT);
}

void RobotFrontend::on_Front_released()
{
    rs->sendCommand(RobotSocket::STOP);
}

void RobotFrontend::on_Back_pressed()
{
    rs->sendCommand(RobotSocket::BACK);
}

void RobotFrontend::on_Back_released()
{
    rs->sendCommand(RobotSocket::STOP);
}

void RobotFrontend::on_Left_pressed()
{
    rs->sendCommand(RobotSocket::LEFT);
}

void RobotFrontend::on_Left_released()
{
    rs->sendCommand(RobotSocket::STOP);
}

void RobotFrontend::on_Right_pressed()
{
    rs->sendCommand(RobotSocket::RIGHT);
}

void RobotFrontend::on_Right_released()
{
    rs->sendCommand(RobotSocket::STOP);
}

void RobotFrontend::on_Stop_clicked()
{
    rs->sendCommand(RobotSocket::STOP);
    enableButtons(true, true);
}

void RobotFrontend::on_Scan_clicked()
{
    rs->sendCommand(RobotSocket::SCAN);
    enableButtons(false, true);
}

void RobotFrontend::on_Connect_clicked()
{
    if(ui->ConnectionStatus->text()=="Disconnected")
    {
        QString ip;
        quint16 port = rs->getPort();
        bool okIP, okPort;

        ip = QInputDialog::getText(this, tr("Robot IP"), tr("Enter robot ip:"), QLineEdit::Normal, rs->getIP(), &okIP);

        if(okIP && !ip.isEmpty()){
            port = QInputDialog::getInt(this, tr("Robot Port"), tr("Enter robot port:"), rs->getPort(), 0, 65535, 1, &okPort);
        }
        if(okPort){
            rs->connectToRobot(ip, port);
        }
    }else if(ui->ConnectionStatus->text()=="Connected"){
        rs->disconnectFromRobot();
    }
}

void RobotFrontend::on_Clear_clicked()
{
    scene->clear();
}

void RobotFrontend::on_Save_clicked()
{
    static int num = 0;
    QStringList filename;
    QPixmap pixMap = QPixmap::grabWidget(ui->Map);
    if(!QDir("Maps").exists())
        QDir().mkdir("Maps");
    if(num == 0){
        QDirIterator dirIt("Maps",QDirIterator::Subdirectories);
        while (dirIt.hasNext()){
            dirIt.next();
            if (QFileInfo(dirIt.filePath()).isFile())
                if (QFileInfo(dirIt.filePath()).suffix() == "png"){
                    int next = QFileInfo(dirIt.filePath()).fileName().split(".").at(0).split("Map").at(1).toInt();
                    (num < next) ? num = next: num;
                }
        }
    }
    ++num;
    pixMap.save("Maps/Map"+QString().number(num)+".png");
    QMessageBox::information(NULL, "Saving", "Map is saved");
}

void RobotFrontend::keyPressEvent(QKeyEvent *keyEvent)
{
    if(ui->ConnectionStatus->text()=="Disconnected")
        return;

    switch (keyEvent->key()) {
    case Qt::Key_W:
        on_Front_pressed();
        break;
    case Qt::Key_A:
        on_Left_pressed();
        break;
    case Qt::Key_S:
        on_Back_pressed();
        break;
    case Qt::Key_D:
        on_Right_pressed();
        break;
    case Qt::Key_Space:
        on_Stop_clicked();
        break;
    default:
        break;
    }
}

void RobotFrontend::keyReleaseEvent(QKeyEvent *keyEvent)
{
    if(ui->ConnectionStatus->text()=="Disconnected")
        return;

    switch (keyEvent->key()) {
    case Qt::Key_W:
        on_Front_released();
        break;
    case Qt::Key_A:
        on_Left_released();
        break;
    case Qt::Key_S:
        on_Back_released();
        break;
    case Qt::Key_D:
        on_Right_released();
        break;
    default:
        break;
    }
}
