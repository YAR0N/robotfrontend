#include "RobotFrontend.h"
#include <QApplication>
#include <QtGui>
#include <QWidget>
#include <QPainter>
#include <QGraphicsScene>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    RobotFrontend w;
    w.show();

    return a.exec();
}
