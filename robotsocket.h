#ifndef ROBOTSOCKET_H
#define ROBOTSOCKET_H

#include <QObject>
#include <QTcpSocket>
#include <QStack>

class RobotSocket : public QObject
{
    Q_OBJECT
public:
    explicit RobotSocket(QObject *parent = 0);
    ~RobotSocket();

    static const char FRONT[];
    static const char BACK[];
    static const char LEFT[];
    static const char RIGHT[];
    static const char STOP[];
    static const char SCAN[];

    QString getIP() const {return _robotIP;}
    quint16 getPort() const {return _robotPort;}

signals:

    void mapDataReceived(int x, int y);
    void robotDataReceived(int x, int y);
    void messageReceived(QString message);
    void socketError(QAbstractSocket::SocketError error);
    void disconnected();
    void connected();

public slots:

    void readData();
    void sendCommand(const char* command);
    void connectToRobot(QString robotIP, quint16 robotPort);
    void disconnectFromRobot();

private slots:

    void auth();
    void optimizeDots();

private:

    class Dot
    {
        public:
            Dot(int x = 0, int y = 0);

            int _x;
            int _y;

            Dot& operator =(const Dot &dot);

            qreal distance(const Dot &dot);

    };

    QString _robotIP;
    quint16 _robotPort;

    QTcpSocket *tcpSocket;

    static const char authCode[];

    static const QRegExp *mapDataRegX;
    static const QRegExp *robotDataRegX;

    QStack<Dot> *_map;
};

#endif // ROBOTSOCKET_H
