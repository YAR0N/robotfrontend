#ifndef RobotFrontend_H
#define RobotFrontend_H

#include <QWidget>
#include <QCoreApplication>
#include <QDebug>
#include <QGraphicsScene>
#include "robotsocket.h"

namespace Ui {
class RobotFrontend;
}

class RobotFrontend : public QWidget
{
    Q_OBJECT

public:
    explicit RobotFrontend(QWidget *parent = 0);
    ~RobotFrontend();

private slots:

    void on_Front_pressed();

    void on_Back_pressed();

    void on_Front_released();

    void on_Back_released();

    void on_Left_pressed();

    void on_Left_released();

    void on_Right_pressed();

    void on_Right_released();

    void on_Stop_clicked();

    void enableButtons(bool enable, bool enableStop);

    void handleConnection();
    void handleDisconnection();

    void showError(QAbstractSocket::SocketError error);

    void drawMap(int x, int y);
    void drawRobotPos(int x, int y);

    void on_Scan_clicked();

    void on_Connect_clicked();

    void on_Clear_clicked();

    void on_Save_clicked();

    void keyPressEvent(QKeyEvent *keyEvent);

    void keyReleaseEvent(QKeyEvent *keyEvent);

private:
    Ui::RobotFrontend *ui;

    QGraphicsScene *scene;

    RobotSocket *rs;
};

#endif // RobotFrontend_H
