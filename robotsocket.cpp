#include "robotsocket.h"
#include <QtCore/qmath.h>

const char RobotSocket::FRONT[] = "FRONT";
const char RobotSocket::BACK[] = "BACK";
const char RobotSocket::LEFT[] = "LEFT";
const char RobotSocket::RIGHT[] = "RIGHT";
const char RobotSocket::STOP[] = "STOP";
const char RobotSocket::SCAN[] = "SCAN";

const char RobotSocket::authCode[] = "USIC";

const QRegExp *RobotSocket::mapDataRegX = new QRegExp("^\\[(-?\\d+)\\/(-?\\d+)\\]$");
const QRegExp *RobotSocket::robotDataRegX = new QRegExp("^R\\[(-?\\d+)\\/(-?\\d+)\\]$");

RobotSocket::RobotSocket(QObject *parent):
    QObject(parent),
    _robotIP(""),
    _robotPort(8880),
    tcpSocket(new QTcpSocket),
    _map(new QStack<Dot>)
{

    connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readData()));
    connect(tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SIGNAL(socketError(QAbstractSocket::SocketError)));

    connect(tcpSocket, SIGNAL(connected()), this, SLOT(auth()));
    connect(tcpSocket, SIGNAL(connected()), this, SIGNAL(connected()));
    connect(tcpSocket, SIGNAL(disconnected()), this, SIGNAL(disconnected()));

}

RobotSocket::~RobotSocket()
{
    tcpSocket->disconnectFromHost();
}

void RobotSocket::connectToRobot(QString robotIP, quint16 robotPort)
{
    _robotIP = robotIP;
    _robotPort = robotPort;

    tcpSocket->connectToHost(_robotIP, _robotPort);
}

void RobotSocket::disconnectFromRobot()
{
    tcpSocket->disconnectFromHost();
}

void RobotSocket::sendCommand(const char *command)
{
    tcpSocket->write(command);
}

void RobotSocket::readData()
{
    QByteArray data = tcpSocket->readAll();
    if(mapDataRegX->exactMatch(data)){
        emit mapDataReceived(mapDataRegX->cap(1).toInt(), mapDataRegX->cap(2).toInt());
        //_map->push(Dot(mapDataRegX->cap(1).toInt(), mapDataRegX->cap(2).toInt()));
    } else if(robotDataRegX->exactMatch(data)){
        emit robotDataReceived(robotDataRegX->cap(1).toInt(), robotDataRegX->cap(2).toInt());
    } else {
        /*if(data == "ENDL"){
            optimizeDots();
        }*/

        emit messageReceived(data);
    }

}

void RobotSocket::optimizeDots()
{
    Dot dot1(0,0);
    Dot dot2(0,0);

    if (!_map->isEmpty()) {
        dot1 = _map->pop();
        emit mapDataReceived(dot1._x, dot1._y);
    }

    while(!_map->isEmpty()){
        dot2 = _map->pop();
        if(!_map->isEmpty() && dot1.distance(dot2) < dot1.distance(_map->top())){
            emit mapDataReceived(dot2._x, dot2._y);
        } else if (_map->isEmpty()) {
            emit mapDataReceived(dot2._x, dot2._y);
        }
        dot1 = dot2;
    }
}

void RobotSocket::auth()
{
    tcpSocket->write(authCode);
}

RobotSocket::Dot::Dot(int x, int y):
    _x(x),
    _y(y)
{
    return;
}

RobotSocket::Dot &RobotSocket::Dot::operator =(const Dot &dot)
{
    _x = dot._x;
    _y = dot._y;

    return *this;
}

qreal RobotSocket::Dot::distance(const Dot &dot)
{
    return qSqrt(qPow(_x - dot._x, 2) + qPow(_y - dot._y, 2));
}
