#-------------------------------------------------
#
# Project created by QtCreator 2014-06-02T11:46:31
#
#-------------------------------------------------

QT       += core gui
QT       += network


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RobotFrontend
TEMPLATE = app


SOURCES += main.cpp\
        RobotFrontend.cpp \
    robotsocket.cpp

HEADERS  += RobotFrontend.h \
    robotsocket.h

FORMS    += RobotFrontend.ui
